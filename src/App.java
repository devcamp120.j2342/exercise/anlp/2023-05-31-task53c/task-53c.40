import models.Circle;
import models.Rectangle;

public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle(3);
        System.out.println(circle1);
        System.out.println(circle1.getArea());
        System.out.println(circle1.getPerimeter());

        Rectangle retangle1 = new Rectangle(10,20);
        System.out.println(retangle1);
        System.out.println(retangle1.getArea());
        System.out.println(retangle1.getPerimeter());
    }
}
